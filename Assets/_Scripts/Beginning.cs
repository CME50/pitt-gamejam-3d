﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class Beginning : MonoBehaviour
{
    public YarnProgram scriptToLoad;
    void Start()
    {
        DialogueRunner dialogueRunner = FindObjectOfType<Yarn.Unity.DialogueRunner>();
        dialogueRunner.Add(scriptToLoad);
        dialogueRunner.StartDialogue("Start");
    }
}
