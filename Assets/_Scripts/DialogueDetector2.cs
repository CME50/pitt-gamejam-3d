﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogueDetector2 : DialogueDetector
{
    public MemoryObject letter;
    void Update()
    {
        if(!isDialogueRunning && letter.visited)
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}
