﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;
using UnityEngine.SceneManagement;

public class DialogueDetector : MonoBehaviour
{
    public MemoryObject picture, DVD, keys, door;
    public YarnProgram scriptToLoad;
    public string sceneToLoad;
    public bool isDialogueRunning = false;
    bool ranFinalDiag = false;
    void Start() 
    {
        door.gameObject.GetComponent<MeshCollider>().enabled = false;
    }
    void Update() 
    {
        if(!isDialogueRunning && !ranFinalDiag)
        {
            if(picture.visited && DVD.visited && keys.visited)
            {
                DialogueRunner dialogueRunner = FindObjectOfType<Yarn.Unity.DialogueRunner>();
                dialogueRunner.Add(scriptToLoad);
                dialogueRunner.StartDialogue("OpenDoor");
                ranFinalDiag = true;
                door.gameObject.GetComponent<MeshCollider>().enabled = true;
            }
        }
        if(!isDialogueRunning && door.visited)
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
    public void setFalse()
    {
        isDialogueRunning = false;
    }
    public void setTrue()
    {
        isDialogueRunning = true;
    }

}
