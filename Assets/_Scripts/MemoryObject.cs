﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class MemoryObject : MonoBehaviour
{
    public string itemName = "";
    public string talkToNode = "";
    public Vector3 interactSpot;
    [Header("Optional")]
    public YarnProgram scriptToLoad;
    public bool visited;

    void Start() 
    {
        visited = false;
        if (scriptToLoad != null) 
        {
                DialogueRunner dialogueRunner = FindObjectOfType<Yarn.Unity.DialogueRunner>();
                dialogueRunner.Add(scriptToLoad);              
        }
    }
}


