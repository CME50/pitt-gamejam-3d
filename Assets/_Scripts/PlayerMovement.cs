﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Yarn.Unity;

public class PlayerMovement : MonoBehaviour
{
    public LayerMask canBeClicked; //creates var for what layer of things are 'walkable'/clickable
    public float padding;
    private NavMeshAgent myAgent; 
    private bool lookingForDialogue = false;
    DialogueRunner dialogueRunner;
    MemoryObject targetMemory;
    public DialogueDetector dialogueDetector;
    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>(); //sets this to agent on player
        dialogueRunner = FindObjectOfType<Yarn.Unity.DialogueRunner>();
    }

    void Update()
    {
        if(dialogueDetector.isDialogueRunning == false)
        {
            if(Input.GetMouseButtonDown(0)) //zero is a right click
            {
                Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition); //set ray from camera to mouse pos
                RaycastHit hitInfo; //info about what ray hits

                if(Physics.Raycast(myRay, out hitInfo, 100, canBeClicked)) //casts ray
                {
                    myAgent.SetDestination(hitInfo.point); //moves player to click position
                    Debug.Log(hitInfo.point);
                }
                if(Physics.Raycast(myRay, out hitInfo))
                {
                    targetMemory = hitInfo.transform.gameObject.GetComponent<MemoryObject>();
                    if(targetMemory != null)
                    {
                        Debug.Log("Hit");
                        myAgent.SetDestination(targetMemory.interactSpot);
                        lookingForDialogue = true;
                    }
                }
            }
        }
        if(lookingForDialogue)
        {
            Debug.Log(Vector3.Distance(myAgent.transform.position, targetMemory.interactSpot));
            if(Vector3.Distance(myAgent.transform.position, targetMemory.interactSpot) < myAgent.stoppingDistance + padding)
            {
                targetMemory.visited = true;
                dialogueRunner.StartDialogue(targetMemory.talkToNode);
                Debug.Log("Running Dialogue");
                lookingForDialogue = false;
            }
        }
    }
}
