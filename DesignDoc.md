**We Three**

**Vision**

We Three is a Isometric Narrative Role-Playing game for Windows where the player experiences Gerry's (main character) flashbacks.
The player explores Gerry's house and interacts with memorable objects that trigger flashbacks to learn about how Gerry could have 
handled his life better, and if he can change it going forward.

**High Level**

*  Gerry is alone due to the culmination of all his past decisions
*  Experience Gerry's past
*  If Gerry could go back and change his decisions, would it matter?


**Baseline Story**

As previously stated, Gerry is the main character who is now alone (single)
his girlfriend has left. He is home alone and sees items that remind him of 
the past. Gerry starts to wonder if he had acted differently, would that matter?
The player takes a walk through Gerry's flashbacks and goes through the
"what if" scenarios.

As Gerry, the player talks to his Brain and his ex, Elizabeth, as he explores
what all went wrong.

**Gameplay**

*  Isometric Top-Down
*  Role Playing
*  Flashbacks
*  Interactable Objects
*  Click Based Movement
*  Main Menu and Credits


**Visuals**

We decided a 3D game would be the best implementation of this idea. The visual 
is to be isomorphic (think Sims, Disco Elysium, Divinity) and it was necessary to see the full furnished the house
to provide a more engaging experience. 

**Touchstones**

* Disco Elysium
* Divinity: Original Sin
* Gone Home
* The Sims
